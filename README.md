### THIS REPOSITORY HAS BEEN MOVED TO GITLAB, YOU CAN FIND IT [**HERE**](https://gitlab.com/xincxfr/unity7-arch)


# unity7-arch
or short "UPFA" (Unity Port For Arch) contains PKGBUILD'S for the Unofficial [**Unity7**](https://gitlab.com/ubuntu-unity/unity) Port to [**Arch Linux**](https://archlinux.org).
These PKGBUILD's only modify the titles/names of packages from Ubuntu although they do not change the logos/branding to not alter much of the Vanilla experience, You can always ask us how to apply it on your side.

### Chat / Discord
Discord: Xinc#0116


## Looking For New Mirrors
UPFA is a free community project. That's why we need your help. If you own a server that could become a mirror of the project, please let us know.

![preview](https://cdn.discordapp.com/attachments/876081532572414013/1029837734635901018/unknown.png)

### Projects that greatly helped in our work (Not directly contributed):

[**The Unity7 Port for Gentoo (Fork)**](https://github.com/c4pp4/gentoo-unity7)

[**The old Unity7 Port for Arch Linux**](https://github.com/chenxiaolong/Unity-for-Arch)

[**The AUR Port for geis**](https://aur.archlinux.org/geis.git)

[**The AUR Port for grail**](https://aur.archlinux.org/grail.git)

[**The AUR Port for frame**](https://aur.archlinux.org/frame.git)

[**The AUR Port for meteo-qt**](https://aur.archlinux.org/meteo-qt.git)

[**The AUR Port for cmake-extras**](https://aur.archlinux.org/cmake-extras.git)

[**The AUR Port for jayatana**](https://aur.archlinux.org/vala-panel-appmenu-jayatana-git.git)

[**The AUR Port for gtk3-ubuntu**](https://aur.archlinux.org/gtk3-ubuntu.git)

